
DELETE FROM civicrm_membership_type WHERE id != 1;

INSERT INTO civicrm_membership_type
(domain_id, name, description, member_of_contact_id, financial_type_id, minimum_fee, duration_unit, duration_interval, period_type, fixed_period_start_day, fixed_period_rollover_day, relationship_type_id, relationship_direction, max_related, visibility, weight, receipt_text_signup, receipt_text_renewal, auto_renew, is_active)
VALUES(1, 'Miembro', 'Miembro del partido.', 1, 2, 100.000000000, 'lifetime', 1, 'rolling', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1);

INSERT INTO civicrm_membership_type
(domain_id, name, description, member_of_contact_id, financial_type_id, minimum_fee, duration_unit, duration_interval, period_type, fixed_period_start_day, fixed_period_rollover_day, relationship_type_id, relationship_direction, max_related, visibility, weight, receipt_text_signup, receipt_text_renewal, auto_renew, is_active)
VALUES(1, 'Simpatizante', 'Simpatizante del partido.', 1, 2, 100.000000000, 'lifetime', 1, 'rolling', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1);

