<?php
use CRM_MembresiaExt_ExtensionUtil as E;

class CRM_MembresiaExt_BAO_Sector extends CRM_MembresiaExt_DAO_Sector {

  /**
   * Create a new Sector based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_MembresiaExt_DAO_Sector|NULL
   *
  public static function create($params) {
    $className = 'CRM_MembresiaExt_DAO_Sector';
    $entityName = 'Sector';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
