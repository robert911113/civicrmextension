<?php
use CRM_MembresiaExt_ExtensionUtil as E;

class CRM_MembresiaExt_BAO_Recinto extends CRM_MembresiaExt_DAO_Recinto {

  /**
   * Create a new Recinto based on array-data
   *
   * @param array $params key-value pairs
   * @return CRM_MembresiaExt_DAO_Recinto|NULL
   *
  public static function create($params) {
    $className = 'CRM_MembresiaExt_DAO_Recinto';
    $entityName = 'Recinto';
    $hook = empty($params['id']) ? 'create' : 'edit';

    CRM_Utils_Hook::pre($hook, $entityName, CRM_Utils_Array::value('id', $params), $params);
    $instance = new $className();
    $instance->copyValues($params);
    $instance->save();
    CRM_Utils_Hook::post($hook, $entityName, $instance->id, $instance);

    return $instance;
  } */

}
