<?php

use CRM_MembresiaExt_ExtensionUtil as E;

/**
 * Form controller class
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/quickform/
 */
class CRM_MembresiaExt_Form_Miembro extends CRM_Core_Form {
  public function buildQuickForm() {
    CRM_Core_Resources::singleton()->addScriptFile('membresia-ext', 'asset/js/models.js');
    CRM_Core_Resources::singleton()->addScriptFile('membresia-ext', 'asset/js/script.js');
    CRM_Core_Resources::singleton()->addScriptFile('membresia-ext', 'asset/js/config-input.js');
    
    $userID = CRM_Core_Session::singleton()->getLoggedInContactID();
    $this->assign('userId', $userID);

    $this->addEntityRef('municipio', ts('Select Municipio'), array(
      'entity' => 'Municipio',
      'placeholder' => ts('- Selecione Municipio -'),
      'select' => array('minimumInputLength' => 0),
      'id' => 'input-municipio',
      'description_field' => 'descripcion'
    ));

    parent::buildQuickForm();
  }

  public function postProcess() {
    parent::postProcess();
  }
}
