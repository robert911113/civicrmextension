<?php
use CRM_MembresiaExt_ExtensionUtil as E;

/**
 * Sector.create API specification (optional)
 * This is used for documentation and validation.
 *
 * @param array $spec description of fields supported by this API call
 * @return void
 * @see https://docs.civicrm.org/dev/en/latest/framework/api-architecture/
 */
function _civicrm_api3_sector_create_spec(&$spec) {
  // $spec['some_parameter']['api.required'] = 1;
}

/**
 * Sector.create API
 *
 * @param array $params
 * @return array API result descriptor
 * @throws API_Exception
 */
function civicrm_api3_sector_create($params) {
  return _civicrm_api3_basic_create(_civicrm_api3_get_BAO(__FUNCTION__), $params);
}

/**
 * Sector.delete API
 *
 * @param array $params
 * @return array API result descriptor
 * @throws API_Exception
 */
function civicrm_api3_sector_delete($params) {
  return _civicrm_api3_basic_delete(_civicrm_api3_get_BAO(__FUNCTION__), $params);
}

/**
 * Sector.get API
 *
 * @param array $params
 * @return array API result descriptor
 * @throws API_Exception
 */
function civicrm_api3_sector_get($params) {
  return _civicrm_api3_basic_get(_civicrm_api3_get_BAO(__FUNCTION__), $params);
}

/**
 * Get municipio list parameters.
 *
 * @see _civicrm_api3_generic_getlist_params
 *
 * @param array $request
 */
function _civicrm_api3_sector_getlist_params(&$request) {
  $fieldsToReturn = array('descripcion', 'id');
  $request['params']['return'] = array_unique(array_merge($fieldsToReturn, $request['extra']));
  $request['params']['options']['sort'] = 'id ASC';
  $request['search_field'] = 'descripcion';

  if (empty($request['params']['id'])) {
    $request['params'] += array(
      'is_template' => 0,
      'is_active' => 1,
    );
  }

  if (!empty($request['input'])) {
    $request['params'][$request['search_field']] = $request['input'];
  }
}

/**
 * Get event list output.
 *
 * @see _civicrm_api3_generic_getlist_output
 *
 * @param array $result
 * @param array $request
 *
 * @return array
 */
function _civicrm_api3_sector_getlist_output($result, $request) {
  $output = array();
  if (!empty($result['values'])) {
    foreach ($result['values'] as $row) {
      $data = array(
        'id' => $row['id'],
        'label' => $row['descripcion']
      );
      
      $data['description'][] = $row['descripcion'];
      
      $repeat = CRM_Core_BAO_RecurringEntity::getPositionAndCount($row['id'], 'civicrm_sector');
      
      $output[] = $data;
    }
  }
  return $output;
}