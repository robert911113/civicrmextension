class Contact {

  constructor($) {
    this.name = $('#input-nombre').val();
    this.lastName = $('#input-apellido').val();
    // this.photoUrl = $('#input-foto')[0].files[0];
  }

  save = async () => {
    return await CRM.api3('Contact', 'create', {
      "display_name": this.name,
      "first_name": this.name,
      "last_name": this.lastName,
      "contact_type": "Individual"
    });
  }
}

class Email {

  constructor($, contactId) {
    this.email = $('#input-email').val();
    this.contactId = contactId;
  }

  save = async () => {
    return CRM.api3('Email', 'create', {
      "contact_id": this.contactId,
      "email": this.email,
      "location_type_id": "3",
      "is_primary": "1"
    });
  }

}

class Phone {

  constructor($, contactId) {
    this.housePhone = $('#input-telefono-residencial').val();
    this.movilPhone = $('#input-telefono-movil').val();
    this.contactId = contactId;
  }

  save = async () => {
    CRM.api3('Phone', 'create', {
      "contact_id": this.contactId,
      "phone": this.housePhone,
      "is_primary": "1",
      "phone_type_id": "1"
    });
    CRM.api3('Phone', 'create', {
      "contact_id": this.contactId,
      "phone": this.movilPhone,
      "is_primary": "0",
      "phone_type_id": "4"
    });
  }
}

class Member {

  constructor($, contactId) {
    this.memberType = $('#input-tipo-miembro').val();
    this.joinDate = $('#input-fecha-entrada').val();
    this.contactId = contactId;
  }

  save = async () => {
    return await CRM.api3('Membership', 'create', {
      "membership_type_id": this.memberType,
      "join_date": this.joinDate,
      "contact_id": this.contactId
    });
  }

}

class MemberExt {

  constructor($, memberId) {
    this.memberId = memberId;
    this.academicLevelId = $('#input-nivel-academico').val();
    this.occupationId = $('#input-ocupacion').val();
    this.civilStateId = $('#input-estado-civil').val();
    this.bloodTypeId = $('#input-tipo-sangre').val();
    this.genreId = $('#input-sexo').val();
  }

  save = async () => {
    return CRM.api3('ExtensionMiembro', 'create', {
      "membership_id": this.memberId,
      "nivelacademico_id": this.academicLevelId,
      "ocupacion_id": this.occupationId,
      "estadocivil_id": this.civilStateId,
      "tiposangre_id": this.bloodTypeId,
      "sexo_id": this.genreId
    });
  }

}

class Address {

  constructor($, memberExtId, addressType) {
    this.memberExtId = memberExtId;
    this.addressType = addressType;

    if (addressType === 1) {
      this.province = $('#input-provincia-persona').val();
      this.circunscripcionId = $('#input-circunscripcion-persona').val();
      this.sectorId = $('#input-sector-persona').val();
      this.municipioId = $('#input-municipio-persona').val();

      this.street = $('#input-calle-persona').val();
      this.nivel = $('#input-nivel-persona').val();
      this.residencial = $('#input-residencial-persona').val();
      this.number = $('#input-numero-persona').val();
    } else if (addressType === 2) {
      this.province = $('#input-provincia-jce').val();
      this.circunscripcionId = $('#input-circunscripcion-jce').val();
      this.sectorId = $('#input-sector-jce').val();
      this.municipioId = $('#input-municipio-jce').val();

      this.street = $('#input-calle-jce').val();
      this.nivel = $('#input-nivel-jce').val();
      this.residencial = $('#input-residencial-jce').val();
      this.number = $('#input-numero-jce').val();

      this.colegioId = $('#input-colegio-jce').val();
      this.recintoId = $('#input-recinto-jce').val();
    }

  }

  save = () => {
    return CRM.api3('Direccion', 'create', {
      "id_miembro_extension": this.memberExtId,
      "id_provincia": this.province,
      "id_tipodireccion": this.addressType,
      "id_circunscripcion": this.circunscripcionId,
      "id_sector": this.sectorId,
      "id_municipio": this.municipioId,
      "id_colegioelectoral": this.colegioId,
      "id_recinto": this.recintoId,
      "calle": this.street,
      "residencial": this.nivel,
      "nivel": this.residencial,
      "numero": this.number
    });
  }

}
