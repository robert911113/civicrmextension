CRM.$(function ($) {

  $('#input-ocupacion').crmEntityRef({
    entity: 'Ocupacion',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-provincia-persona').crmEntityRef({
    entity: 'Provincia',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-municipio-persona').crmEntityRef({
    entity: 'Municipio',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-circunscripcion-persona').crmEntityRef({
    entity: 'Circunscripcion',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-sector-persona').crmEntityRef({
    entity: 'Sector',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-provincia-persona').crmEntityRef({
    entity: 'Provincia',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-provincia-jce').crmEntityRef({
    entity: 'Provincia',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-municipio-jce').crmEntityRef({
    entity: 'Municipio',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-circunscripcion-jce').crmEntityRef({
    entity: 'Circunscripcion',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-sector-jce').crmEntityRef({
    entity: 'Sector',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-provincia-jce').crmEntityRef({
    entity: 'Provincia',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-recinto-jce').crmEntityRef({
    entity: 'Recinto',
    select: {
      minimumInputLength: 0
    }
  });

  $('#input-colegio-jce').crmEntityRef({
    entity: 'ColegioElectoral',
    select: {
      minimumInputLength: 0
    }
  });
});
