CRM.$(function ($) {
  // @TODO convert image to base64
  // $('#input-foto').change((element) => {
  //   var file = element.target.files[0];
  //   var reader = new FileReader();

  //   reader.onloadend = function () {
  //     console.log('RESULT', reader.result);
  //     $('#img-foto').attr('src', reader.result);
  //   }

  //   reader.readAsDataURL(file);
  // });

  $('#button-submit').click(async (e) => {
    try {
      if (document.getElementById('Miembro').checkValidity()) {
        e.preventDefault();

        $("#btnSubmit").attr("disabled", true);

        const {
          id: contactId
        } = await new Contact($).save();
        await new Email($, contactId).save();
        await new Phone($, contactId).save();
        const {
          id: memberId
        } = await new Member($, contactId).save();
        const {
          id: memberExtId
        } = await new MemberExt($, memberId).save();
        await new Address($, memberExtId, 1).save();
        await new Address($, memberExtId, 2).save();

        CRM.alert("El miembro ha sido guardado con exito.", "Guardado", 'success');

        $("#btnSubmit").attr("disabled", false);
      } else {
        CRM.alert("Rellenar los campos requeridos.", "Guardado", 'warning');
      }
    } catch (e) {
      console.log(e);
    }
  });
});
