<?php
// This file declares a new entity type. For more details, see "hook_civicrm_entityTypes" at:
// http://wiki.civicrm.org/confluence/display/CRMDOC/Hook+Reference
return array (
  0 => 
  array (
    'name' => 'Recinto',
    'class' => 'CRM_MembresiaExt_DAO_Recinto',
    'table' => 'civicrm_recinto',
  ),
);
