{crmAPI var='memberTypes' entity='MembershipType' action='get'}
{crmAPI var='genres' entity='Sexo' action='get'}
{crmAPI var='typeBloods' entity='TipoSangre' action='get'}
{crmAPI var='civilStates' entity='EstadoCivil' action='get'}
{crmAPI var='academicLevels' entity='NivelAcademico' action='get'}
{crmAPI var='empadronador' entity='Contact' action='get' id=$userId}

<form id="form-miembro">
  <div >
    <div>
      <h1>Nuevo Miembro</h1>
      <div>
        Ingresar Cedula sin guiones:
      </div>
      <div class="input-group-lg">
        <input type="text" class="input-group" id="QCMiembro" name="QCMiembro" value="" required>
        <button type="button" name="smcedula" id="btnQCMiembro">Buscar</button>
      </div>
    </div>
    <br>
    <br>
  <div class="datos-contact">
    <div class="row">
      <img id="img-foto" class="previsualizar col-sm-3" width="120px" height="120px">
      <br>
      <input id="input-foto" type="file" name="imgMember" class="col-sm-4 imgMiembro" value="">
    </div>
    <input type="text"  placeholder="Nombre" class="big crm-form-text" readonly>
    <input type="text"  placeholder="Cedula" class="big crm-form-text" readonly>
    <span class="highlight-header">Tipo:</span>
    <select id="input-tipo-miembro" class="big crm-form-select">
      {foreach from=$memberTypes.values item=membershipType}
        <option value="{$membershipType.id}">{$membershipType.name}</option>
      {/foreach}
    </select>
        <span class="highlight-header">Miembro desde:</span>
        <input id="input-fecha-entrada" class="big crm-form-text" type="date"  value="" require>
  </div>
  <br>
      <div class="crm-accordion-wrapper crm-contactDetails-accordion">
        <div class="crm-accordion-header">
          <p>Datos Generales</p>
        </div>
        <div class="crm-accordion-body">
          <table class="form-layout-compressed">
            <td>
              <div class="highlight-header">Cedula:</div>
              <input id="input-cedula" type="text"  value="" required class="big crm-form-text"> <br>

              <div class="highlight-header">Nombre:</div>
              <input id="input-nombre" type="text"  value="" required class="big crm-form-text"><br>

              <div class="highlight-header">Apellido:</div>
              <input id="input-apellido" type="text"  value="" required class="big crm-form-text"><br>

              <div class="highlight-header">Telefono Residencial:</div>
              <input id="input-telefono-residencial" type="text"  value="" required class="big crm-form-text"> <br>

              <div class="highlight-header">Telefono Movil:</div>
              <input id="input-telefono-movil" type="text"  value="" required class="big crm-form-text"><br>

              <div class="highlight-header">Email:</div>
              <input id="input-email" type="email"  value="" require><br>
            </td>
          <td>
            <div class="highlight-header">Lugar de Nacimiento:</div>
            <input id="input-lugar-nacimiento" type="text"  value="" required class="big crm-form-text"> <br>
            <div class="highlight-header">Fecha de Nacimiento:</div>
            <input id="input-fecha-nacimiento" type="date"  value="" class="big crm-form-text" require><br>
            <div class="highlight-header">Sexo:</div>
            <select id="input-sexo" class="big crm-form-select">
                <option disabled selected>Selecionar</option>
                {foreach from=$genres.values item=sexo}
                  <option value="{$sexo.id}">{$sexo.descripcion}</option>
                {/foreach}
            </select>
            <br>
            
            <div class="highlight-header">Tipo de Sangre:</div>
            <select id="input-tipo-sangre" class="big crm-form-select">
                <option disabled selected>Selecionar</option>
                {foreach from=$typeBloods.values item=tiposangre}
                  <option value="{$tiposangre.id}">{$tiposangre.descripcion}</option>
                {/foreach}
            </select>
            <br>
            
            <div class="highlight-header">Estado Civil:</div>
            <select id="input-estado-civil" class="big crm-form-select">
                <option  disabled selected>Selecionar</option>
                {foreach from=$civilStates.values item=estadocivil}
                  <option value="{$estadocivil.id}">{$estadocivil.descripcion}</option>
                {/foreach}
            </select>
            <br>

            <div class="highlight-header">Nivel Academico:</div>
            <select id="input-nivel-academico" class="big crm-form-select">
                <option disabled selected>Selecionar</option>
                {foreach from=$academicLevels.values item=nivelacademico}
                  <option value="{$nivelacademico.id}">{$nivelacademico.descripcion}</option>
                {/foreach}
            </select>
            <br>
          </td>
          <td>
            <div class="highlight-header">Ocupacion:</div>
            <input id="input-ocupacion" placeholder="{ts}- Selecionar Ocupacion -{/ts}"/>
            <br>
            <div class="highlight-header">Dirigente:</div>
            <select class="big crm-form-select">
                <option disabled selected>Selecionar</option>
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
            <br>
            <div class="highlight-header">Empadronador:</div>
            {foreach from=$empadronador.values item=contact}
              <input id="input-empadronador" type="text" data-contactId="{$userId}" value="{$contact.display_name}" required disabled class="big crm-form-text"><br>
            {/foreach}
           
            <div class="highlight-header">Nivel de Compromiso:</div>
            <select  class="big crm-form-select">
                <option disabled selected>Selecionar</option>
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
            <br>

            <div class="highlight-header">Estatus:</div>
            <select  class="big crm-form-select">
                <option disabled selected>Selecionar</option>
                <option value="Si">Si</option>
                <option value="No">No</option>
            </select>
            <br>
          </td>
          </table>
        </div>
      </div>
      <div class="crm-accordion-wrapper crm-contactDetails-accordion">
        <div class="crm-accordion-header">
          <p>Detalles de Direccion</p>
        </div>
        <div class="crm-accordion-body">
          <table class="form-layout-compressed">
              <tr>
                <th>Informacion Personal</th>
                <th>Informacion de JCE</th>
                <th></th>
              </tr>

              <tr>
                <td>
                  <div class="highlight-header">Provincia:</div>
                  <input id="input-provincia-persona" placeholder="{ts}- Selecionar Provincia -{/ts}" required/>
                  <br>

                  <div class="highlight-header">Municipio:</div>
                  <input id="input-municipio-persona" placeholder="{ts}- Selecionar Municipio -{/ts}" required/>
                  <br>
                
                  <div class="highlight-header">Circunscripcion:</div>
                  <input id="input-circunscripcion-persona" placeholder="{ts}- Selecionar Circunscripcion -{/ts}" required/>
                  <br>
                
                  <div class="highlight-header">Sector:</div>
                  <input id="input-sector-persona" placeholder="{ts}- Selecionar Sector -{/ts}" required/>
                  <br>

                  <div class="highlight-header">Calle:</div>
                  <input id="input-calle-persona" type="text" required class="big crm-form-text"><br>

                  <div class="highlight-header">Residencial:</div>
                  <input id="input-residencial-persona" type="text" required class="big crm-form-text"><br>

                  <div class="highlight-header">Nivel:</div>
                  <input id="input-nivel-persona" type="text" required class="big crm-form-text"> <br>

                  <div class="highlight-header">Numero:</div>
                  <input id="input-numero-persona" type="text" required class="big crm-form-text"><br>
              </td>

              <td>
                <div class="highlight-header">Provincia:</div>
                <input id="input-provincia-jce" placeholder="{ts}- Selecionar Provincia -{/ts}" required/>
                <br>
                
                <div class="highlight-header">Municipio:</div>
                <input id="input-municipio-jce" placeholder="{ts}- Selecionar Municipio -{/ts}" required/>
                <br>

                <div class="highlight-header">Circunscripcion:</div>
                <input id="input-circunscripcion-jce" placeholder="{ts}- Selecionar Circunscripcion -{/ts}" required/>
                <br>

                <div class="highlight-header">Sector:</div>
                <input id="input-sector-jce" placeholder="{ts}- Selecionar Sector -{/ts}" required/>
                <br>
                
                <div class="highlight-header">Recinto:</div>
                <input id="input-recinto-jce" placeholder="{ts}- Selecionar Recinto -{/ts}" required/>
                <br>

                <div class="highlight-header">Colegio electoral:</div>
                <input id="input-colegio-jce" placeholder="{ts}- Selecionar Colegio -{/ts}" required/>
                <br>

                <div class="highlight-header">Calle:</div>
                <input id="input-calle-jce" type="text" required class="big crm-form-text">
                <br>

                <div class="highlight-header">Residencial:</div>
                <input id="input-residencial-jce" type="text" required class="big crm-form-text">
                <br>
              </td>
              <td>
                <div class="highlight-header">Nivel:</div>
                <input id="input-nivel-jce" type="text" required class="big crm-form-text"><br>

                <div class="highlight-header">Numero:</div>
                <input id="input-numero-jce" type="text" required class="big crm-form-text"><br>

                <div class="highlight-header">Geo-Ubicacion:</div>
                <input id="input-geo-jce" type="text" required class="big crm-form-text"><button id="btnUbication"><i class="fa fa-map"></i></button><br>
              </td>
              </tr>

          </table>
        </div>
      </div>
      <div class="crm-accordion-wrapper crm-contactDetails-accordion">
        <div class="crm-accordion-header">
          <p>Imagen Cedula</p>
        </div>
        <div class="crm-accordion-body">
          <table class="form-layout-compressed">
            <tr>
              <th>Parte Frontal</th>
              <th>Parte Trasera </th>
            </tr>
            <tr>
              <td>
                <img src="" class="srcCedulaFrontal col-sm-3" width="500px" height="275px">
                <input type="file" name="Foto" class="imgCedulaFrontal" value="">
              </td>
              <td>
                <img src="" class="srcCedulaTrasera col-sm-3" width="500px" height="275px">
                <input type="file" name="Foto" class="imgCedulaTrasera" value="">
              </td>
            </tr>
          </table>
        </div>
      </div>
    <div class="modal-footer">
      <input id="button-submit" type="submit" value="Enviar Formulario">
    </div>
  </div>
</form>